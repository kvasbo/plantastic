var added = 0;
var stupidWords = ["Waste", "Compiling", "Surfing", "Making bugs", "Sleeping", "Dancing", "Meetings"];
var unitsize = 50;

$( document ).ready(function() {

	initFunctions();
	updateBoardStatus();
	

});

function initFunctions(){
	
	$("#btn_create").off().on("click", function(ev, el)
	{
		var text = $("#title").val();
		var height = $("select[name='height']").val();
		var width = $("select[name='width']").val();
		if(text == "") text = stupidWords[added%stupidWords.length];
		
		addItem(text, height, width);
		
	});
	
	$(".addbuttonS").off().on("click", function(ev, el)
	{
		var text = $("#title").val();
		
		var height = $(ev.currentTarget).attr("height");
		var width =  $(ev.currentTarget).attr("width");
		if(text == "") text = stupidWords[added%stupidWords.length];
		
		addItem(text, width, height);

	});
	
	$( "#capacityarea" ).resizable({
	   grid: 50,
	   minWidth: 250,
	   minHeight: 100,
	   resize: function( event, ui ) {
	   		var devs = Math.round(ui.size.height / 50) - 1;
	   		var weeks = Math.round(ui.size.width / 50);
	   		
			$("#capacitylegend").html(devs+" developers, "+weeks+" weeks").attr("devs", devs).attr("weeks", weeks);
			updateBoardStatus();
	   }
	 }).draggable({grid: [ 50,50 ] });
	  
	 
	$("#export").on("click", function(){
		doExport();
	});
	
	
	$("#import").on("click", function(){
		$("#in").fadeIn();
	});
	
	
	$("#exportclose").on("click", function() {
		$("#out").fadeOut();
	});
	
	$("#importCancel").on("click", function() {
		$("#in").fadeOut();
	});
	
	$("#importDo").on("click", function() {
		doImport();
	});
	
	$("#buttons").droppable({
		accept: ".item",
		drop: function( event, ui ) {
		        ui.draggable.fadeOut().remove();
		      },
		activeClass: "buttonbardelete"
	});
	
	$("#formen").droppable({
		accept: ".item",
		drop: function( event, ui ) {
		        ui.draggable.fadeOut().remove();
		      },
		activeClass: "buttonbardelete"
	});
	
	$(window).bind('keypress', function(e) {
	
	if (document.activeElement.nodeName != 'TEXTAREA' || document.activeElement.nodeName != 'INPUT') {

		//console.log(e.keyCode);
		
		if(e.keyCode==124){
			$(".item").toggle(100);
		}
	
		
	}
	
	

});
		
}

function updateBoardStatus()
{
	var weeksTot = 0;
	var cap = 0;
	var util = 0;
	
	//Get weeks on board
	$(".item").each(function(el,data){
		var weeks = $(this).attr("width") * $(this).attr("height");
		weeksTot = weeksTot + weeks;
	});
	
	//Get capacity
	var cap = $("#capacitylegend").attr("devs") * $("#capacitylegend").attr("weeks");
	
	if(cap == 0) util = "100";
	else {
		util = Math.round((weeksTot / cap)*100);
	}
	
	
	
	$("#status").html("<table class='capTab'><tr><td>On board:</td><td>"+weeksTot+"</td></tr><tr><td>Capacity:</td><td>"+cap+"</td></tr><tr><td>Utilisation:</td><td>"+util+"%</td></tr></table>");
}

function initDraggable(){

	$( ".item" ).draggable({snap: ".snap" }).css("cursor", "pointer");
	
}

function addItem(text, height, width)
{
	added++;
	var weeks = Math.round(height*width);
	var h_real = (height * unitsize) - 20; 
	var w_real = width * unitsize - 25; 
	var add = '<div id="item_'+added+'" text="'+text+'" height="'+height+'" width="'+width+'" class="item snap">'+text+' ('+weeks+')</div>';
	$("#staging").append(add);
	
	var selector = "#item_"+added;
	
	$(selector).width(w_real).height(h_real);
	
	nextColor("item_"+added);
	
	initDraggable();
	
	updateBoardStatus();
}


function doExport()
{	
	var out = [];
	
	$(".item").each(function(el) {
		var tmp = {};
		tmp.title = $(this).attr("text");	
		tmp.height = $(this).attr("height");
		tmp.width = $(this).attr("width");
		out.push(tmp);
	});
	
	var jsonOut = JSON.stringify(out);
	
	$("#outArea").html(jsonOut)
	
	$("#out").fadeIn();
		
}

function doImport()
{
	var inData = $("#inArea").val();
	
	//alert(inData);
	
	try{
		
		var newData = JSON.parse(inData);
		
		for(i=0; i < newData.length; i++)
		{
			if(newData[i].title && newData[i].height &&  newData[i].width)
			{
				addItem(newData[i].title, newData[i].height, newData[i].width);
			}
		}
		
		initDraggable();	
		$("#in").fadeOut();
	}
	catch(e)
	{
		console.log(e);
		alert("Sorry mac, something was wrong!");
	}
}	

function nextColor(id)
{
	var s = "#"+id;
	var newClass = "color_"+(added%5);
	$(s).removeClass("color_0 color_1 color_2 color_3 color_4").addClass(newClass);
}